// ==UserScript==
// @name               [C9x3] Libre Front-Ends.
// @description        Tries to redirect Non-Libre Software to Libre Software Front-Ends for Non-Libre Software.
// @match       	   *://*/*
// @version            1.2
// @run-at             document-start
// @grant              none
// ProjectHomepage     https://c9x3.neocities.org/
// @downloadURL  	   https://gitlab.com/c9x3/libre-front-ends/-/raw/main/Libre_Front-Ends.js
// @updateURL    	   https://gitlab.com/c9x3/libre-front-ends/-/raw/main/Libre_Front-Ends.js
// ==/UserScript==

// When working with code, make changes ONE AT A TIME and save frequently! I was having issues with this UserScript not being detected recently. Also make sure you have your, "};"s!

// Make a log in the console when the script starts.

console.log('Libre Front-Ends has started!')

// Don’t run in frames.

if (window.top !== window.self)

return;

var currentURL = location.href;

// Reddit //

// https://github.com/spikecodes/libreddit
// AGPL3 License.

//if (currentURL.match("www.reddit.com")) {

//	location.href = location.href.replace("www.reddit.com", "libreddit.domain.glass");

//};

// https://old.reddit.com/
// Proprietary.

if (currentURL.match("www.reddit.com")) {

	location.href = location.href.replace("www.reddit.com", "old.reddit.com/");

};

// https://codeberg.org/teddit/teddit
// AGPL3 License.

//if (currentURL.match("reddit.com")) {

//	location.href = location.href.replace("reddit.com", "teddit.net");

//};



// Wikipedia //

// https://simple.wikipedia.org/wiki/Main_Page
// CC-SA-4.0 License. (For text.)
// https://github.com/wikimedia/mediawiki/blob/master/COPYING GPL2 (For Code.)

//if (currentURL.match("www.wikipedia.org")) {

	//location.href = location.href.replace("www.wikipedia.org", "simple.wikipedia.org");

//};

//if (currentURL.match("en.wikipedia.org")) {

	//location.href = location.href.replace("en.wikipedia.org", "simple.wikipedia.org");

//};



// Quora //

// https://github.com/zyachel/quetre
// AGPL3 License.

if (currentURL.match("www.quora.com")) {

	location.href = location.href.replace("www.quora.com", "quetre.iket.me");

};



// Twitter //

// https://github.com/zedeus/nitter
// AGPL3 License.

if (currentURL.match("twitter.com")) {

	location.href = location.href.replace("twitter.com", "nitter.net");

};



// Google Translate //

// https://github.com/TheDavidDelta/lingva-translate
// AGPL3 License.

if (currentURL.match("translate.google.com")) {

	location.href = location.href.replace("translate.google.com", "lingva.ml");

};



// YouTube //

// https://github.com/iv-org/invidious <- Main repository for this project.
// https://github.com/unixfox/invidious-custom <- Current instances repository. (Hosted in the Netherlands.)
// https://docs.invidious.io/instances/ <- Public instances.
// https://flagpedia.net/index <- A way to identify flags.
// yewtu.be
// vid.puffyan.us
// AGPL3 License.

if (currentURL.match("www.youtube.com")) {

location.href = location.href.replace("www.youtube.com", "vid.puffyan.us");

};

//if (currentURL.match("vid.puffyan.us")) {

//location.href = location.href.replace("vid.puffyan.us", "yewtu.be");

//};

if (currentURL.match("yewtu.be")) {

location.href = location.href.replace("yewtu.be", "vid.puffyan.us");

};


// TikTok //

// Try to replace, "www.tiktok.com" with, "https://proxitok.herokuapp.com."
// https://github.com/pablouser1/ProxiTok
// AGPL3 License.

if (currentURL.match("www.tiktok.com")) {

	location.href = location.href.replace("www.tiktok.com", "proxitok.herokuapp.com");

};

//



// Rimgo //

// https://codeberg.org/video-prize-ranch/rimgo
// https://imgur.com/gallery/6GzPTi5 -> https://rimgo.pussthecat.org/gallery/6GzPTi5
// AGPL3

if (currentURL.match("imgur.com")) {

	location.href = location.href.replace("imgur.com", "rimgo.pussthecat.org");

};



// Librarian //

// https://codeberg.org/librarian/librarian
// https://librarian.pussthecat.org/
// AGPL3
// https://odysee.com/@rossmanngroup:a -> https://librarian.pussthecat.org/@rossmanngroup

if (currentURL.match("odysee.com")) {

	location.href = location.href.replace("odysee.com", "librarian.pussthecat.org");

};

//


